import "./App.scss";

// React Router
import { BrowserRouter, Routes, Route } from "react-router-dom";

// Redux store
import { Provider } from "react-redux";
import { mainStore } from "./store";

// Pages
import HomePage from "./pages/home";
import MainLayoutComponent from "./components/main-layout";
import EventPage from "./pages/event";
import FeedPage from "./pages/feed";
import LoginPage from "./pages/login";
import ProfilePage from "./pages/profile";
import NotFoundPage from "./pages/not-found";

const App = () => {
  return (
    <Provider store={mainStore}>
      <div className="app-layout font-face-ssp">
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/dashboard" element={<MainLayoutComponent />}>
              <Route path="feed" element={<FeedPage />} />
              <Route path="event/:eventId" element={<EventPage />} />
              <Route path="profile" element={<ProfilePage />} />
            </Route>
            <Route path="*" element={<NotFoundPage />} />
          </Routes>
        </BrowserRouter>
      </div>
    </Provider>
  );
};

export default App;
