export default interface EventContentHeaderInterface {
  channel_name: string;
  create_time: string;
  creator: {
    id: number;
    username: string;
    email: string;
    avatar: string;
  };
  title: string;
}
