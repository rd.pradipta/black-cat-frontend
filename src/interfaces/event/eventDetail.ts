export default interface EventContentDetailInterface {
  start_time: string;
  end_time: string;
  location: string;
  description: string;
  images: string[];
}
