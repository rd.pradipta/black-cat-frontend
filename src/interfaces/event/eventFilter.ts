export default interface EventFilterInterface {
  startime: string;
  end_time: string;
  channel_id: number;
}
