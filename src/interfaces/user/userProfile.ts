export default interface UserProfileInterface {
  id: number;
  fullname: string;
  username: string;
  email: string;
  avatar: string;
}
