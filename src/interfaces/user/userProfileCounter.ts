export default interface UserProfileCounterInterface {
  likes_count: number;
  participants_count: number;
  pasts_count: number;
}
