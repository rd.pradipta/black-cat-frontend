import moment from "moment";

const dateFormat = "DD MMMM YYYY HH:mm";
const dateOnly = "DD MMMM YYYY";
const timeOnly = "HH:mm";
const identifierOnly = "a";

// Convert ISO Format Received From Backend
const convertISODateToRegularDate = (dateToChange: string, variant: number) => {
  const timeVariants = [dateFormat, dateOnly, timeOnly, identifierOnly];
  return moment(dateToChange).utc().format(timeVariants[variant]);
};

// To get time info -> X Days Ago
const getTimeDifferencesFromNow = (dateToChange: string) => {
  return moment(dateToChange).utc().fromNow();
};

// Show Comment Bottom Bar & Hide Regular Bar
const showCommentBar = () => {
  const bottomBarDiv = document.getElementById("bottom-bar-regular");
  bottomBarDiv?.classList.add("hide-content");
  const commentBarDiv = document.getElementById("bottom-bar-comment");
  commentBarDiv?.classList.remove("hide-content");
};

// Show Regular Bottom Bar & Hide Comment Bar
const showRegularBottomBar = () => {
  const commentBarDiv = document.getElementById("bottom-bar-comment");
  commentBarDiv?.classList.add("hide-content");
  const bottomBarDiv = document.getElementById("bottom-bar-regular");
  bottomBarDiv?.classList.remove("hide-content");
};

// Show Event Complete Description After Click SHOW ALL Button
const showEventCompleteDesc = () => {
  const descDiv = document.getElementById("event-detail-desc-txt");
  descDiv?.classList.remove("event-detail-desc-txt-overflow");
  const blurContainer = document.getElementById(
    "event-detail-desc-blur-container"
  );
  blurContainer?.classList.add("hide-content");
};

const hideSidebar = () => {
  const sidebarDiv = document.getElementById("app-sidebar");
  sidebarDiv?.classList.add("hide-content");
};

const showSidebar = () => {
  const sidebarDiv = document.getElementById("app-sidebar");
  sidebarDiv?.classList.remove("hide-content");
};

export default {
  convertISODateToRegularDate,
  getTimeDifferencesFromNow,
  showCommentBar,
  showRegularBottomBar,
  showEventCompleteDesc,
  hideSidebar,
  showSidebar,
};
