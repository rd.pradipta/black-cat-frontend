// Value constants
enum ValueConstant {
  TOAST_TIMEOUT = 3000,
  TOAST_SHORT_TIMEOUT = 1500,
}

export default ValueConstant;
