// Error code constants
enum ErrorCodeConstant {
  HTTP_404 = 404,
  HTTP_500 = 500,
}

export default ErrorCodeConstant;
