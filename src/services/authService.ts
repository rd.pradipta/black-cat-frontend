import requestHeader from "../helpers/requestHeader";
import LoginInterface from "../interfaces/auth";
import StringConstant from "../helpers/constants/stringConstant";

const login = (payload: LoginInterface) => {
  return requestHeader.post("/auth/token", payload).then(response => {
    localStorage.setItem(
      StringConstant.KEY_LOCAL_STORAGE_LOGIN,
      JSON.stringify(response.data)
    );
    return response.data;
  });
};

const logout = () => {
  localStorage.removeItem(StringConstant.KEY_LOCAL_STORAGE_LOGIN);
};

export default {
  login,
  logout,
};
