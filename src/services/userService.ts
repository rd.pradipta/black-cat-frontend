import requestHeader from "../helpers/requestHeader";

// id -> User ID

const getUserProfile = (id: string) => {
  return requestHeader.get("/user/".concat(id));
};

const getUserProfileCounter = (id: string) => {
  return requestHeader.get("/user/".concat(id).concat("/counters"));
};

const getParticipatedEvents = (id: string) => {
  return requestHeader.get("/user/".concat(id).concat("/participants"));
};

const getLikedEvents = (id: string) => {
  return requestHeader.get("/user/".concat(id).concat("/likes"));
};

export default {
  getUserProfile,
  getUserProfileCounter,
  getParticipatedEvents,
  getLikedEvents,
};
