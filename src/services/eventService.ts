import requestHeader from "../helpers/requestHeader";

// id -> Event ID

const getAll = (userId: string) => {
  return requestHeader.get("/events/".concat(userId));
};

const getHeader = (id: string) => {
  return requestHeader.get("/event/".concat(id).concat("/headers"));
};

const getDetail = (id: string) => {
  return requestHeader.get("/event/".concat(id).concat("/details"));
};

const getParticipants = (id: string) => {
  return requestHeader.get("/event/".concat(id).concat("/participants"));
};

const getLikes = (id: string) => {
  return requestHeader.get("/event/".concat(id).concat("/likes"));
};

const getComments = (id: string) => {
  return requestHeader.get("/event/".concat(id).concat("/comments"));
};

const getChannels = () => {
  return requestHeader.get("/channels/");
};

const getUserLikeAndParticipation = (eventId: string, userId: string) => {
  return requestHeader.get(
    "/event/".concat(eventId).concat("/activities/").concat(userId)
  );
};

const addParticipant = (id: string, payload: any) => {
  return requestHeader.post(
    "/event/".concat(id).concat("/participants"),
    payload
  );
};

const addLike = (id: string, payload: any) => {
  return requestHeader.post("/event/".concat(id).concat("/likes"), payload);
};

const addComment = (id: string, commentPayload: any) => {
  return requestHeader.post(
    "/event/".concat(id).concat("/comments"),
    commentPayload
  );
};

const removeParticipant = (id: string, userId: string) => {
  return requestHeader.delete(
    "/event/".concat(id).concat("/participants/").concat(userId)
  );
};

const removeLike = (id: string, userId: string) => {
  return requestHeader.delete(
    "/event/".concat(id).concat("/likes/").concat(userId)
  );
};

export default {
  getAll,
  getHeader,
  getDetail,
  getParticipants,
  getLikes,
  getComments,
  getChannels,
  getUserLikeAndParticipation,
  addParticipant,
  addLike,
  addComment,
  removeParticipant,
  removeLike,
};
