// Reducer for search filter
import { SEARCH_EVENT } from "../actions/types";
import NumberConstant from "../helpers/constants/numberConstant";

const initialState = {
  startTime: "",
  endTime: "",
  channel: NumberConstant.ZERO,
};

export default function searchEventReducer(state = initialState, action: any) {
  const { type } = action; // Coming soon will add "payload"
  switch (type) {
    case SEARCH_EVENT:
      return {
        ...state,
      };
    default:
      return state;
  }
}
