// The Auth reducer will update the isLoggedIn and user state of the application
import { LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT } from "../actions/types";
import StringConstant from "../helpers/constants/stringConstant";

const userLS = JSON.parse(
  localStorage.getItem(StringConstant.KEY_LOCAL_STORAGE_LOGIN) || "{}"
);

const initialState =
  Object.keys(userLS).length !== 0
    ? { isLoggedIn: true, userLS }
    : { isLoggedIn: false, userLS: null };

export default function authReducer(state = initialState, action: any) {
  const { type, payload } = action;
  switch (type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        user: payload.user, // This .user is taken from user state on authAction
      };
    case LOGIN_FAIL:
      return {
        ...state,
        isLoggedIn: false,
        user: null,
      };
    case LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        user: null,
      };
    default:
      return state;
  }
}
