// Reducer for reply comment
import { REPLY_COMMENT } from "../actions/types";

const initialState = { usernameToReply: "" };

export default function replyCommentReducer(state = initialState, action: any) {
  const { type, payload } = action;
  switch (type) {
    case REPLY_COMMENT:
      return { usernameToReply: payload };
    default:
      return state;
  }
}
