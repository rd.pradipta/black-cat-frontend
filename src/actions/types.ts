// Action Types for Redux
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT = "LOGOUT";
export const SET_MESSAGE = "SET_MESSAGE";
export const CLEAR_MESSAGE = "REMOVE_MESSAGE";
export const REPLY_COMMENT = "REPLY_COMMENT";
export const SEARCH_EVENT = "SEARCH_EVENT";
