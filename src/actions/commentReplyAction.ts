// Message Action Creator
import { REPLY_COMMENT } from "./types";

const replyComment = (usernameToReply: string) => {
  return {
    type: REPLY_COMMENT,
    payload: usernameToReply,
  };
};

export default {
  replyComment,
};
