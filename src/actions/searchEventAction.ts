// Message Action Creator
import { SEARCH_EVENT } from "./types";
import EventFilterInterface from "../interfaces/event/eventFilter";

const searchEvent = (payloadData: EventFilterInterface) => {
  // Still a skeleton, will add the functionality later
  return {
    type: SEARCH_EVENT,
    payload: payloadData,
  };
};

export default {
  searchEvent,
};
