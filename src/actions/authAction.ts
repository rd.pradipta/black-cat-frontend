// Auth Action Creator
import { LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT, SET_MESSAGE } from "./types";
import LoginInterface from "../interfaces/auth";
import authService from "../services/authService";
import StringConstant from "../helpers/constants/stringConstant";
import ErrorCodeConstant from "../helpers/constants/errorCodeConstant";

export const login = (payloadData: LoginInterface) => {
  return (dispatch: any) => {
    let message: string;
    return authService
      .login(payloadData)
      .then((data: string) => {
        dispatch({
          type: LOGIN_SUCCESS,
          payload: { user: data }, // "user" is the state name
        });
        return Promise.resolve();
      })
      .catch((error: any) => {
        if (!error.response) message = StringConstant.TOAST_NO_CONNECTION;
        else if (error.response.status === ErrorCodeConstant.HTTP_404)
          message = StringConstant.TOAST_USER_NOT_FOUND_LOGIN;
        dispatch({
          type: LOGIN_FAIL,
        });
        dispatch({
          type: SET_MESSAGE,
          payload: message,
        });
        return Promise.reject();
      });
  };
};

export const logout = () => {
  return (dispatch: any) => {
    authService.logout();
    dispatch({
      type: LOGOUT,
    });
  };
};
