import "./event.scss";
import EventHeaderComponent from "../../components/event/event-header";
import EventContentComponent from "../../components/event/event-content";

const EventPage = () => {
  return (
    <>
      <EventHeaderComponent />
      <EventContentComponent />
    </>
  );
};

export default EventPage;
