import "./login.scss";
import { useState } from "react";
import { Navigate } from "react-router-dom";
import ToastComponent from "../../components/toast";

// Redux
import { login } from "../../actions/authAction";
import { useAppDispatch, useAppSelector } from "../../hook";

// Assets
import StringConstant from "../../helpers/constants/stringConstant";
import streetDanceImg from "../../assets/images/street-dance.png";
import logoCatImg from "../../assets/icons/logo-cat.svg";

const LoginPage = () => {
  const [showToastData, setShowToast] = useState(false);
  const [loading, setLoading] = useState(false);
  const [credentialsInfo, setCredentials] = useState({
    username: "",
    password: "",
  });

  // Redux variables
  const dispatch = useAppDispatch();
  const { isLoggedIn } = useAppSelector(state => {
    return state.authReducer;
  });
  const { message } = useAppSelector(state => {
    return state.messageReducer;
  });

  const handleChange = (event: any) => {
    setCredentials({
      ...credentialsInfo,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = () => {
    setLoading(true);
    dispatch(login(credentialsInfo)).finally(() => {
      showToast();
      setLoading(false);
    });
  };

  const showToast = () => {
    setShowToast(true);
  };

  const hideToast = () => {
    setShowToast(false);
  };

  if (isLoggedIn) {
    return <Navigate to="/dashboard/feed" />;
  }

  let loginBtnToShow;
  if (credentialsInfo.username && credentialsInfo.password) {
    loginBtnToShow = (
      <button
        type="button"
        className="login-btn login-btn-active"
        onClick={handleSubmit}
      >
        {loading ? (
          <span className="spinner-border spinner-border-lg" />
        ) : (
          <span>
            <h4>{StringConstant.SIGN_IN}</h4>
          </span>
        )}
      </button>
    );
  } else {
    loginBtnToShow = (
      <button disabled type="button" className="login-btn login-btn-inactive">
        <span>
          <h4>{StringConstant.SIGN_IN}</h4>
        </span>
      </button>
    );
  }

  return (
    <>
      <ToastComponent show={showToastData} hideToast={hideToast}>
        {message}
      </ToastComponent>
      <div className="login-bg-container">
        <img className="login-bg" src={streetDanceImg} alt="login-bg" />
        <div className="login-bg-layer" />
        <div className="title-container">
          <h3 className="title-text">{StringConstant.LOGIN_UPPER_TITLE}</h3>
          <h1 className="title-text title-bottom">
            {StringConstant.LOGIN_BOTTOM_TITLE}
          </h1>
          <div className="login-logo-circle" />
          <img
            className="login-logo green-filter"
            src={logoCatImg}
            alt="logo-cat-circle"
          />
          <br />
          <div className="login-form-container">
            <div>
              <input
                type="text"
                className="login-text-input icon-username"
                name="username"
                placeholder="Username"
                value={credentialsInfo.username}
                onChange={handleChange}
              />
            </div>
            <div>
              <input
                type="password"
                className="login-text-input icon-password"
                name="password"
                placeholder="Password"
                value={credentialsInfo.password}
                onChange={handleChange}
              />
            </div>
          </div>
        </div>
        {loginBtnToShow}
      </div>
    </>
  );
};

export default LoginPage;
