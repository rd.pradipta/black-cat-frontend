import { shallow } from "enzyme";
import { Provider } from "react-redux";
import { mainStore } from "../../store";
import LoginPage from ".";

describe("Test Login Page Component", () => {
  it("Component renders without crashing", () => {
    shallow(
      <Provider store={mainStore}>
        <LoginPage />
      </Provider>
    );
  });
});
