import "./feed.scss";
import { useEffect, useState } from "react";
import NumberConstant from "../../helpers/constants/numberConstant";
import StringConstant from "../../helpers/constants/stringConstant";
import LoadingComponent from "../../components/loading";
import EventCardComponent from "../../components/card/event-card";
import eventService from "../../services/eventService";
import NoActivityComponent from "../../components/no-activity";

const FeedPage = () => {
  let componentToShow;
  // To give maximum 5 pages to follow the requirement
  const [currentPageIndexLoaded, setPageIndexLoaded] = useState(
    NumberConstant.ZERO
  );
  const [loading, setLoading] = useState(true);
  const [isLoadingMoreEvents, setLoadingMoreEvents] = useState(false);
  const [events, setEvents] = useState<any[]>([]);

  const localStorageData = JSON.parse(
    localStorage.getItem(StringConstant.KEY_LOCAL_STORAGE_LOGIN) || "{}"
  );
  const currentUserId = localStorageData.user_id;

  const getEvents = async (isRefetch: boolean) => {
    if (!isRefetch) setLoading(true);
    await eventService
      .getAll(currentUserId)
      .then(({ data }) => {
        if (!isRefetch) setEvents(data);
        else {
          setEvents(prevState => {
            // Add page index that have been loaded by using infinite scroll
            setPageIndexLoaded(currentPageIndexLoaded + NumberConstant.ONE);
            return [...prevState, ...data];
          });
          setLoadingMoreEvents(false);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };

  // Inject using onScroll event
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      return window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  // Get initial data using API
  useEffect(() => {
    getEvents(false);
  }, []);

  // Fetch more data after scroll reach bottom of page
  useEffect(() => {
    if (!isLoadingMoreEvents || currentPageIndexLoaded > NumberConstant.FIVE) {
      setLoadingMoreEvents(false);
      return;
    }
    getEvents(true);
  }, [isLoadingMoreEvents]);

  const handleScroll = () => {
    const totalHeight = window.innerHeight + document.documentElement.scrollTop;
    const { offsetHeight } = document.documentElement;
    if (-Math.round(-totalHeight) !== offsetHeight || isLoadingMoreEvents)
      return;
    setLoadingMoreEvents(true);
  };

  const eventArr: any = [];
  const buildArrOfEventCard = () => {
    for (
      let i = NumberConstant.ZERO;
      i < events.length;
      i += NumberConstant.ONE
    ) {
      eventArr.push(
        <EventCardComponent
          key={i}
          eventData={events[i]}
          currentUserId={currentUserId}
        />
      );
    }
    return eventArr;
  };

  if (loading) componentToShow = <LoadingComponent />;
  if (!loading && events.length === NumberConstant.ZERO)
    componentToShow = <NoActivityComponent />;
  else if (!loading && events.length)
    componentToShow = (
      <>
        <div>{buildArrOfEventCard()}</div>
        {isLoadingMoreEvents && <LoadingComponent />}
      </>
    );
  return <div>{componentToShow}</div>;
};

export default FeedPage;
