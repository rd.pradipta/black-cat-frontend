import { shallow } from "enzyme";
import NotFoundPage from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<NotFoundPage />);
});

describe("<NotFoundPage /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("Should render cat image", () => {
    expect(wrapper.find("img")).toHaveLength(1);
  });

  it("Should render not found text", () => {
    expect(wrapper.find("h5")).toHaveLength(1);
  });

  it("Should render home button", () => {
    expect(wrapper.find("button")).toHaveLength(1);
  });
});
