import "./not-found.scss";
import { Link } from "react-router-dom";
import StringConstant from "../../helpers/constants/stringConstant";
import logoCatImg from "../../assets/icons/logo-cat.svg";

const NotFoundPage = () => {
  return (
    <>
      <div className="center-container">
        <img className="cat-logo" src={logoCatImg} alt="cat-img" />
      </div>
      <div className="center-container">
        <h5 className="msg-not-found">{StringConstant.PAGE_NOT_FOUND}</h5>
      </div>
      <Link to="/" className="remove-link-decoration">
        <div className="center-container">
          <button type="button" className="btn-home">
            Back to Home Page
          </button>
        </div>
      </Link>
    </>
  );
};

export default NotFoundPage;
