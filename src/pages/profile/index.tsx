import "./profile.scss";
import ProfileHeaderComponent from "../../components/profile/profile-header";
import ProfileContentComponent from "../../components/profile/profile-content";

const ProfilePage = () => {
  return (
    <>
      <ProfileHeaderComponent />
      <ProfileContentComponent />
    </>
  );
};

export default ProfilePage;
