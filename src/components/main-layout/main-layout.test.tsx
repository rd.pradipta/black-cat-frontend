import { shallow } from "enzyme";
import { Provider } from "react-redux";
import { mainStore } from "../../store";
import MainLayoutComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(
    <Provider store={mainStore}>
      <MainLayoutComponent />
    </Provider>
  );
});

describe("<MainLayoutComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
