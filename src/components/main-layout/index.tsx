import "./main-layout.scss";
import { Navigate, useOutlet } from "react-router-dom";
import SidebarComponent from "../sidebar";
import TopNavComponent from "../top-nav";
import StringConstant from "../../helpers/constants/stringConstant";
import { useAppSelector } from "../../hook";

const MainLayoutComponent = () => {
  const outlet = useOutlet();
  const { isLoggedIn } = useAppSelector(state => {
    return state.authReducer;
  });

  const localStorageData = JSON.parse(
    localStorage.getItem(StringConstant.KEY_LOCAL_STORAGE_LOGIN) || "{}"
  );
  const currentUser = localStorageData.user;

  if (!isLoggedIn) {
    return <Navigate to="/login" />;
  }

  return (
    <div className="flex-container">
      <SidebarComponent />
      <div>
        <TopNavComponent avatarLink={currentUser.avatar} />
        {outlet}
      </div>
    </div>
  );
};

export default MainLayoutComponent;
