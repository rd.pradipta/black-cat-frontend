import "./profile-header.scss";
import { useEffect, useState } from "react";
import StringConstant from "../../../helpers/constants/stringConstant";
import userService from "../../../services/userService";
import UserProfileInterface from "../../../interfaces/user/userProfile";
import emailImg from "../../../assets/icons/email.svg";

const ProfileHeaderComponent = () => {
  const [loading, setLoading] = useState(true);
  const [userProfile, setProfile] = useState<UserProfileInterface | null>(null);

  const localStorageData = JSON.parse(
    localStorage.getItem(StringConstant.KEY_LOCAL_STORAGE_LOGIN) || "{}"
  );
  const currentUserId = localStorageData.user_id;

  const getUserProfile = async () => {
    setLoading(true);
    await userService
      .getUserProfile(currentUserId)
      .then(({ data }) => {
        setProfile(data);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getUserProfile();
  }, []);

  let pageContent;
  if (loading) {
    pageContent = (
      <>
        <span className="spinner-border user-spinner" />
        <p className="profile-loading-text">
          {StringConstant.PLEASE_WAIT_LOADING}
        </p>
      </>
    );
  } else {
    pageContent = (
      <>
        <img
          className="user-img-profile"
          src={userProfile?.avatar}
          alt="user-img"
        />
        <p className="user-username">{userProfile?.username}</p>
        <div className="user-email-container">
          <img className="email-icon" src={emailImg} alt="email-icon" />
          <p className="user-email">{userProfile?.email}</p>
        </div>
      </>
    );
  }

  return <div className="page-container">{pageContent}</div>;
};

export default ProfileHeaderComponent;
