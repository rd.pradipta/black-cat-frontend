import { shallow } from "enzyme";
import ProfileHeaderComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<ProfileHeaderComponent />);
});

describe("<ProfileHeaderComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
