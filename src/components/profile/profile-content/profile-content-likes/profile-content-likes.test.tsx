import { shallow } from "enzyme";
import ProfileContentLikes from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<ProfileContentLikes />);
});

describe("<ProfileContentLikes /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
