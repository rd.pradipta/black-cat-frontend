import "./profile-content-likes.scss";
import { useEffect, useState } from "react";
import StringConstant from "../../../../helpers/constants/stringConstant";
import NumberConstant from "../../../../helpers/constants/numberConstant";
import LoadingComponent from "../../../loading";
import EventCardComponent from "../../../card/event-card";
import NoActivityComponent from "../../../no-activity";
import userService from "../../../../services/userService";

const ProfileContentLikes = () => {
  let componentToShow;
  const [loading, setLoading] = useState(true);
  const [events, setEvents] = useState([]);

  const localStorageData = JSON.parse(
    localStorage.getItem(StringConstant.KEY_LOCAL_STORAGE_LOGIN) || "{}"
  );
  const currentUserId = localStorageData.user_id;

  const getEvents = async () => {
    setLoading(true);
    await userService
      .getLikedEvents(currentUserId)
      .then(({ data }) => {
        setEvents(data);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getEvents();
  }, []);

  const eventArr = [];
  if (!loading) {
    for (
      let i = NumberConstant.ZERO;
      i < events.length;
      i += NumberConstant.ONE
    ) {
      eventArr.push(
        <EventCardComponent
          key={i}
          eventData={events[i]}
          currentUserId={currentUserId}
        />
      );
    }
  }

  if (loading) componentToShow = <LoadingComponent />;
  if (!loading && events.length === NumberConstant.ZERO)
    componentToShow = <NoActivityComponent />;
  else if (!loading && events.length) componentToShow = eventArr;

  return <div>{componentToShow}</div>;
};

export default ProfileContentLikes;
