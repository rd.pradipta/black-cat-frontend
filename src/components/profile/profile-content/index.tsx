import "./profile-content.scss";
import { useState, useEffect } from "react";
import ProfileContentLikes from "./profile-content-likes";
import ProfileContentParticipants from "./profile-content-participants";
import ProfileContentPast from "./profile-content-past";
import StringConstant from "../../../helpers/constants/stringConstant";
import userService from "../../../services/userService";
import UserProfileCounterInterface from "../../../interfaces/user/userProfileCounter";

// Assets
import likeUntouchedImg from "../../../assets/icons/like-outline.svg";
import goingUntouchedImg from "../../../assets/icons/check-outline.svg";
import pastUntouchedImg from "../../../assets/icons/past-outline.svg";
import likeTouchedImg from "../../../assets/icons/like.svg";
import goingTouchedImg from "../../../assets/icons/check.svg";
import pastTouchedImg from "../../../assets/icons/past.svg";

const ProfileContentComponent = () => {
  const [loading, setLoading] = useState(true);
  const [userCounter, setCounter] =
    useState<UserProfileCounterInterface | null>(null);

  const localStorageData = JSON.parse(
    localStorage.getItem(StringConstant.KEY_LOCAL_STORAGE_LOGIN) || "{}"
  );
  const currentUserId = localStorageData.user_id;

  const getUserProfileCounter = async () => {
    setLoading(true);
    await userService
      .getUserProfileCounter(currentUserId)
      .then(({ data }) => {
        setCounter(data);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  useEffect(() => {
    getUserProfileCounter();
  }, []);

  let userCounters: number[];
  if (loading) userCounters = [0, 0, 0];
  else
    userCounters = [
      userCounter?.likes_count || 0,
      userCounter?.participants_count || 0,
      userCounter?.pasts_count || 0,
    ];

  let profileSectionToShow;
  let likeButton;
  let goingButton;
  let pastButton;

  const tabButtonMaker = (btnType: string, isEnable: boolean) => {
    const enabledImgs = [likeTouchedImg, goingTouchedImg, pastTouchedImg];
    const disabledImgs = [
      likeUntouchedImg,
      goingUntouchedImg,
      pastUntouchedImg,
    ];
    const states = [
      { likes: 1, going: 0, past: 0 },
      { likes: 0, going: 1, past: 0 },
      { likes: 0, going: 0, past: 1 },
    ];
    let chooseIdx = 0;
    let btnText;
    if (btnType === StringConstant.LIKES_SMALL) {
      chooseIdx = 0;
      btnText = StringConstant.LIKES;
    } else if (btnType === StringConstant.PARTICIPANT_SMALL) {
      chooseIdx = 1;
      btnText = StringConstant.PARTICIPANT;
    } else {
      chooseIdx = 2;
      btnText = StringConstant.PAST;
    }
    const enabledButton = (
      <div
        className="tab-button"
        onClick={() => {
          return changeButtonState(states[chooseIdx]);
        }}
      >
        <img
          className="tab-button-img green-filter-btn-img"
          src={enabledImgs[chooseIdx]}
          alt="enabled-img"
        />
        <p className="tab-button-txt green-filter-btn-txt">
          {userCounters[chooseIdx]} {btnText}
        </p>
      </div>
    );
    const disabledButton = (
      <div
        className="tab-button"
        onClick={() => {
          return changeButtonState(states[chooseIdx]);
        }}
      >
        <img
          className="tab-button-img gray-filter-btn-img"
          src={disabledImgs[chooseIdx]}
          alt="disabled-img"
        />
        <p className="tab-button-txt">
          {userCounters[chooseIdx]} {btnText}
        </p>
      </div>
    );
    if (isEnable) return enabledButton;
    return disabledButton;
  };

  // Initial state for the active tab
  const [buttonState, changeButtonState] = useState({
    likes: 1,
    going: 0,
    past: 0,
  });

  if (buttonState.likes) {
    profileSectionToShow = <ProfileContentLikes />;
    likeButton = <>{tabButtonMaker("likes", true)}</>;
    goingButton = <>{tabButtonMaker("going", false)}</>;
    pastButton = <>{tabButtonMaker("past", false)}</>;
  } else if (buttonState.going) {
    profileSectionToShow = <ProfileContentParticipants />;
    likeButton = <>{tabButtonMaker("likes", false)}</>;
    goingButton = <>{tabButtonMaker("going", true)}</>;
    pastButton = <>{tabButtonMaker("past", false)}</>;
  } else if (buttonState.past) {
    profileSectionToShow = <ProfileContentPast />;
    likeButton = <>{tabButtonMaker("likes", false)}</>;
    goingButton = <>{tabButtonMaker("going", false)}</>;
    pastButton = <>{tabButtonMaker("past", true)}</>;
  }

  return (
    <>
      <div className="sticky-tab">
        <div className="line-separator horizontal-line" />
        <div className="flex-container flex-justify-content tab-container">
          {likeButton}
          <div className="line-separator vertical-line-not-full" />
          {goingButton}
          <div className="line-separator vertical-line-not-full" />
          {pastButton}
        </div>
        <div className="line-separator horizontal-line" />
      </div>
      <div className="footer-content-bg footer-content-height">
        {profileSectionToShow}
      </div>
    </>
  );
};

export default ProfileContentComponent;
