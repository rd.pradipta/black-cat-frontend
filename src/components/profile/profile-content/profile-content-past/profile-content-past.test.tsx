import { shallow } from "enzyme";
import ProfileContentPast from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<ProfileContentPast />);
});

describe("<ProfileContentPast /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
