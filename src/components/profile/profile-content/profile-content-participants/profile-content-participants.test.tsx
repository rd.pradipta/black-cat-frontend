import { shallow } from "enzyme";
import ProfileContentParticipants from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<ProfileContentParticipants />);
});

describe("<ProfileContentParticipants /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
