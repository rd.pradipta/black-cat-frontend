/* eslint-disable @typescript-eslint/no-unused-vars */
import "./sidebar.scss";
import { useEffect, useState } from "react";
import NumberConstant from "../../helpers/constants/numberConstant";
import eventService from "../../services/eventService";
import StringConstant from "../../helpers/constants/stringConstant";
import searchImg from "../../assets/icons/search.svg";

const SidebarComponent = () => {
  const [loading, setLoading] = useState(true);
  const [channels, setChannel] = useState<any[]>([]);
  const [choosenChannel, setChoosenChannel] = useState(0);
  const [startDate, setStartDate] = useState("");
  const [endData, setEndDate] = useState("");

  const getChannels = async () => {
    setLoading(true);
    await eventService
      .getChannels()
      .then(({ data }) => {
        setChannel(data);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getChannels();
  }, []);

  const setTimeFilter = (fitlerId: number) => {
    // Will do the filter here
  };

  // Channel ID = 0 -> All. 1 - N will be the real number to query
  const chooseChannel = (channelId: number) => {
    setChoosenChannel(channelId);
    deactivateOtherButtons(channelId, "channel");
    activateCorrespondingButton(channelId, "channel");
  };

  const chooseTimeFilter = (filterId: number) => {
    setTimeFilter(filterId);
    deactivateOtherButtons(filterId, "time");
    activateCorrespondingButton(filterId, "time");
  };

  const deactivateOtherButtons = (idToExclude: number, filterParam: string) => {
    if (filterParam === "channel") {
      for (
        let i = NumberConstant.ZERO;
        i < channels.length + NumberConstant.ONE;
        i += NumberConstant.ONE
      ) {
        if (i !== idToExclude) {
          const currChannelDiv = document.getElementById(
            "channel-btn-".concat(String(i))
          );
          currChannelDiv?.classList.remove("sidebar-btn-selected");
        }
      }
    } else {
      for (
        let i = NumberConstant.ZERO;
        i < NumberConstant.SEVEN;
        i += NumberConstant.ONE
      ) {
        if (i !== idToExclude) {
          const currTimeFilterDiv = document.getElementById(
            "time-filter-btn-".concat(String(i))
          );
          currTimeFilterDiv?.classList.remove("sidebar-btn-selected");
        }
      }
    }
  };

  const activateCorrespondingButton = (
    idToInclude: number,
    filterParam: string
  ) => {
    if (filterParam === "channel") {
      const currChannelDiv = document.getElementById(
        "channel-btn-".concat(String(idToInclude))
      );
      currChannelDiv?.classList.add("sidebar-btn-selected");
    } else {
      const currTimeFilterDiv = document.getElementById(
        "time-filter-btn-".concat(String(idToInclude))
      );
      currTimeFilterDiv?.classList.add("sidebar-btn-selected");
    }
  };

  const sendFilterData = () => {
    // Ini akan send data filter untuk dilakukan filtering
  };

  const generateTimeFilterButtons = () => {
    const txtData = [
      StringConstant.TIME_FILTER_ANYTIME,
      StringConstant.TIME_FILTER_TODAY,
      StringConstant.TIME_FILTER_TOMORROW,
      StringConstant.TIME_FILTER_THIS_WEEK,
      StringConstant.TIME_FILTER_THIS_MONTH,
      StringConstant.TIME_FILTER_LATER,
    ];
    const timeFilterBtnArr = [];
    for (
      let i = NumberConstant.ZERO;
      i < txtData.length;
      i += NumberConstant.ONE
    ) {
      timeFilterBtnArr.push(
        <div
          key={i}
          id={"time-filter-btn-".concat(String(i))}
          onClick={() => {
            return chooseTimeFilter(i);
          }}
          className="sidebar-buttons-common-style sidebar-time-filter-section-btn-each"
        >
          {txtData[i]}
        </div>
      );
    }
    return timeFilterBtnArr;
  };

  const generateChannelButtons = () => {
    channelBtnArr = [
      <div
        key={NumberConstant.ZERO}
        id={"channel-btn-".concat(String(NumberConstant.ZERO))}
        className="sidebar-buttons-common-style sidebar-channel-section-btn-each"
        onClick={() => {
          return chooseChannel(0);
        }}
      >
        {StringConstant.ALL_SMALL}
      </div>,
    ];
    for (
      let i = NumberConstant.ONE;
      i < channels.length + NumberConstant.ONE;
      i += NumberConstant.ONE
    ) {
      channelBtnArr.push(
        <div
          key={i}
          id={"channel-btn-".concat(String(i))}
          onClick={() => {
            return chooseChannel(channels[i - NumberConstant.ONE].id);
          }}
          className="sidebar-buttons-common-style sidebar-channel-section-btn-each"
        >
          {channels[i - NumberConstant.ONE].name}
        </div>
      );
    }
    return channelBtnArr;
  };

  let timeFilterBtnArr: any = [];
  let channelBtnArr: any = [];
  if (!loading) {
    timeFilterBtnArr = generateTimeFilterButtons();
    channelBtnArr = generateChannelButtons();
  }

  return (
    <div id="app-sidebar" className="sidebar-container hide-content">
      <div className="sidebar-up-container">
        <div>
          <div className="sidebar-section-title">
            {StringConstant.DATE_SECTION_SIDEBAR}
          </div>
          <div className="flex-container flex-wrap sidebar-section-btn-list-container">
            {timeFilterBtnArr}
          </div>
        </div>
        <div>
          <div className="sidebar-section-title">
            {StringConstant.CHANNEL_SECTION_SIDEBAR}
          </div>

          <div className="flex-container flex-wrap sidebar-section-btn-list-container">
            {channelBtnArr}
          </div>
        </div>
      </div>
      <div className="sidebar-bottom-container active-search-btn">
        <div className="sidebar-search-btn">
          <img
            className="search-icon active-search-icon"
            src={searchImg}
            alt="search-icon"
          />
          <p className="search-txt active-search-txt">
            {StringConstant.SEARCH_BUTTON_SIDEBAR}
          </p>
        </div>
        <p className="search-desc">All Activities</p>
      </div>
    </div>
  );
};

export default SidebarComponent;
