/* eslint-disable react/no-array-index-key */
import "./toast.scss";
import ReactDOM from "react-dom";
import { useState, useEffect } from "react";
import ValueConstant from "../../helpers/constants/valueContant";

const ToastComponent = (props: any) => {
  const [node] = useState(document.createElement("div"));
  const removeNode = () => {
    if (document.querySelector("#toast-element")?.children.length) {
      document.querySelector("#toast-element")?.childNodes[0].remove();
    }
  };
  useEffect(() => {
    if (props.show) {
      document
        .querySelector("#toast-element")
        ?.appendChild(node)
        .classList.add("toast-container-custom");
      setTimeout(() => {
        removeNode();
        props.hideToast();
      }, ValueConstant.TOAST_TIMEOUT);
    } else {
      removeNode();
    }
    return () => {
      return removeNode();
    };
  }, [node, props]);
  return ReactDOM.createPortal(props.children, node);
};

export default ToastComponent;
