import { shallow } from "enzyme";
import LoadingComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<LoadingComponent />);
});

describe("<LoadingComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("Should render spinner", () => {
    expect(wrapper.find("span")).toHaveLength(1);
  });

  it("Should render loading text", () => {
    expect(wrapper.find("p")).toHaveLength(1);
  });
});
