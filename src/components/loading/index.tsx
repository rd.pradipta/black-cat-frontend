import "./loading.scss";
import StringConstant from "../../helpers/constants/stringConstant";

const LoadingComponent = () => {
  return (
    <div className="parent">
      <div className="child">
        <span className="spinner-border user-spinner" />
        <p className="profile-loading-text">
          {StringConstant.PLEASE_WAIT_LOADING}
        </p>
      </div>
    </div>
  );
};

export default LoadingComponent;
