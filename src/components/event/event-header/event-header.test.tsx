import { shallow } from "enzyme";
import EventHeaderComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<EventHeaderComponent />);
});

describe("<EventHeaderComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
