/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "./event-header.scss";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import EventContentHeaderInterface from "../../../interfaces/event/eventContentHeader";
import LoadingComponent from "../../loading";
import helperFunctions from "../../../helpers/helperFunctions";
import StringConstant from "../../../helpers/constants/stringConstant";

// Service
import eventService from "../../../services/eventService";

const EventHeaderComponent = () => {
  let componentToShow;
  const urlParam = useParams();
  const [loading, setLoading] = useState(true);
  const [eventHeader, setHeader] = useState<EventContentHeaderInterface | null>(
    null
  );

  const getPostedTime = (dateToChange: string | undefined) => {
    if (dateToChange)
      return helperFunctions.getTimeDifferencesFromNow(dateToChange);
    return "";
  };

  const getEventHeaderInfo = async () => {
    setLoading(true);
    await eventService
      .getHeader(urlParam.eventId!)
      .then(({ data }) => {
        setHeader(data);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getEventHeaderInfo();
  }, []);

  if (loading) componentToShow = <LoadingComponent />;
  else
    componentToShow = (
      <div className="event-header-container">
        <div className="flex-container">
          <div className="channel-name-container">
            {eventHeader?.channel_name}
          </div>
        </div>
        <br />
        <h3 className="event-header-title">{eventHeader?.title}</h3>
        <div className="flex-container event-header-author-info-container">
          <img
            className="event-header-author-photos"
            src={eventHeader?.creator.avatar}
            alt="author-img"
          />
          <div className="event-header-author-name-date-container">
            <div className="event-header-author-username">
              {eventHeader?.creator.username}
            </div>
            <div className="event-header-author-posted-time">
              {StringConstant.PUBLISHED}{" "}
              {getPostedTime(eventHeader?.create_time)}{" "}
            </div>
          </div>
        </div>
        <div />
      </div>
    );
  return <div>{componentToShow}</div>;
};

export default EventHeaderComponent;
