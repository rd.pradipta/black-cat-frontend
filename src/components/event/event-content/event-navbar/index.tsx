import "./event-navbar.scss";
import { useState } from "react";
import StringConstant from "../../../../helpers/constants/stringConstant";

// Assets
import detailUntouchedImg from "../../../../assets/icons/info-outline.svg";
import participantUntouchedImg from "../../../../assets/icons/people-outline.svg";
import commentUntouchedImg from "../../../../assets/icons/past-outline.svg";

import detailTouchedImg from "../../../../assets/icons/info.svg";
import participantTouchedImg from "../../../../assets/icons/people.svg";
import commentTouchedImg from "../../../../assets/icons/past.svg";

const EventNavbarComponent = () => {
  let detailButton;
  let participantButton;
  let commentButton;

  const tabButtonMaker = (btnType: string, isEnable: boolean) => {
    const enabledImgs = [
      detailTouchedImg,
      participantTouchedImg,
      commentTouchedImg,
    ];
    const disabledImgs = [
      detailUntouchedImg,
      participantUntouchedImg,
      commentUntouchedImg,
    ];
    const states = [
      { details: 1, participants: 0, comments: 0 },
      { details: 0, participants: 1, comments: 0 },
      { details: 0, participants: 0, comments: 1 },
    ];
    let chooseIdx = 0;
    let btnText;
    if (btnType === StringConstant.EVENT_TAB_DETAILS_SMALL) {
      chooseIdx = 0;
      btnText = StringConstant.EVENT_TAB_DETAILS;
    } else if (btnType === StringConstant.EVENT_TAB_PARTICIPANTS_SMALL) {
      chooseIdx = 1;
      btnText = StringConstant.EVENT_TAB_PARTICIPANTS;
    } else {
      chooseIdx = 2;
      btnText = StringConstant.EVENT_TAB_COMMENTS;
    }
    const enabledButton = (
      <div
        className="tab-button"
        onClick={() => {
          return changeButtonState(states[chooseIdx]);
        }}
      >
        <img
          className="tab-button-img green-filter-btn-img"
          src={enabledImgs[chooseIdx]}
          alt="enabled-img"
        />
        <p className="tab-button-txt green-filter-btn-txt">{btnText}</p>
      </div>
    );
    const disabledButton = (
      <div
        className="tab-button"
        onClick={() => {
          return changeButtonState(states[chooseIdx]);
        }}
      >
        <img
          className="tab-button-img gray-filter-btn-img"
          src={disabledImgs[chooseIdx]}
          alt="disabled-img"
        />
        <p className="tab-button-txt">{btnText}</p>
      </div>
    );
    if (isEnable) return enabledButton;
    return disabledButton;
  };

  // Initial state for the active tab
  const [buttonState, changeButtonState] = useState({
    details: 1,
    participants: 0,
    comments: 0,
  });

  if (buttonState.details) {
    detailButton = <>{tabButtonMaker("details", true)}</>;
    participantButton = <>{tabButtonMaker("participants", false)}</>;
    commentButton = <>{tabButtonMaker("comments", false)}</>;
  } else if (buttonState.participants) {
    detailButton = <>{tabButtonMaker("details", false)}</>;
    participantButton = <>{tabButtonMaker("participants", true)}</>;
    commentButton = <>{tabButtonMaker("comments", false)}</>;
  } else if (buttonState.comments) {
    detailButton = <>{tabButtonMaker("details", false)}</>;
    participantButton = <>{tabButtonMaker("participants", false)}</>;
    commentButton = <>{tabButtonMaker("comments", true)}</>;
  }

  const jumpToReleventDiv = (elementIdDestination: string) => {
    const releventDiv = document.getElementById(elementIdDestination);
    releventDiv?.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <div className="sticky-tab">
      <div className="line-separator horizontal-line" />
      <div className="flex-container flex-justify-content tab-container">
        <div
          onClick={() => {
            return jumpToReleventDiv("event-content-detail-section");
          }}
          className="event-content-btn-centered"
        >
          {detailButton}
        </div>
        <div className="line-separator vertical-line-not-full" />
        <div
          onClick={() => {
            return jumpToReleventDiv("event-content-participant-section");
          }}
          className="event-content-btn-centered"
        >
          {participantButton}
        </div>
        <div className="line-separator vertical-line-not-full" />
        <div
          onClick={() => {
            return jumpToReleventDiv("event-content-comment-section");
          }}
          className="event-content-btn-centered"
        >
          {commentButton}
        </div>
      </div>
      <div className="line-separator horizontal-line" />
    </div>
  );
};

export default EventNavbarComponent;
