import { shallow } from "enzyme";
import { Provider } from "react-redux";
import { mainStore } from "../../../../store";
import EventBottomBar from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(
    <Provider store={mainStore}>
      <EventBottomBar />
    </Provider>
  );
});

describe("<EventBottomBar /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
