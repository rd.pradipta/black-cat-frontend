/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "./event-bottom-bar.scss";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import helperFunctions from "../../../../helpers/helperFunctions";
import ToastComponent from "../../../toast";
import EventUserActivityInterface from "../../../../interfaces/event/eventUserActivity";
import eventService from "../../../../services/eventService";
import StringConstant from "../../../../helpers/constants/stringConstant";
import ValueConstant from "../../../../helpers/constants/valueContant";

// Redux
import { useAppDispatch, useAppSelector } from "../../../../hook";
import { setMessage } from "../../../../actions/messageAction";

// Assets
import commentUntouchedImg from "../../../../assets/icons/comment-single.svg";
import likeUntouchedImg from "../../../../assets/icons/like-outline.svg";
import goingUntouchedImg from "../../../../assets/icons/check-outline.svg";
import likeTouchedImg from "../../../../assets/icons/like.svg";
import goingTouchedImg from "../../../../assets/icons/check.svg";

const EventBottomBar = () => {
  const urlParam = useParams();
  const [showToastData, setShowToast] = useState(false);
  const [userActivity, setActivity] =
    useState<EventUserActivityInterface | null>(null);

  const localStorageData = JSON.parse(
    localStorage.getItem(StringConstant.KEY_LOCAL_STORAGE_LOGIN) || "{}"
  );
  const currentUserId = localStorageData.user_id;

  const payloadToSend = {
    user_id: currentUserId,
  };

  const getUserLikeAndParticipationActivity = async () => {
    await eventService
      .getUserLikeAndParticipation(urlParam.eventId!, currentUserId)
      .then(({ data }) => {
        setActivity(data);
      });
  };

  useEffect(() => {
    getUserLikeAndParticipationActivity();
  }, []);

  // Redux variables
  const dispatch = useAppDispatch();
  const { message } = useAppSelector(state => {
    return state.messageReducer;
  });

  // Reload
  const reloadPage = () => {
    return window.location.reload();
  };

  // Actions related to participation & likes
  const addParticipant = async () => {
    await eventService
      .addParticipant(urlParam.eventId!, payloadToSend)
      .then(() => {
        goingBtnToShow = goingClickedBtn;
        dispatch(setMessage(StringConstant.TOAST_PARTICIPANT_ADD));
        showToast();
      });
  };

  const removeParticipant = async () => {
    await eventService
      .removeParticipant(urlParam.eventId!, currentUserId)
      .then(() => {
        goingBtnToShow = goingUnclickedBtn;
        dispatch(setMessage(StringConstant.TOAST_PARTICIPANT_REMOVE));
        showToast();
      });
  };

  const addLike = async () => {
    await eventService.addLike(urlParam.eventId!, payloadToSend).then(() => {
      likeBtnToShow = likeClickedBtn;
      dispatch(setMessage(StringConstant.TOAST_LIKE_ADD));
      showToast();
    });
  };

  const removeLike = async () => {
    await eventService.removeLike(urlParam.eventId!, currentUserId).then(() => {
      likeBtnToShow = likeUnclickedBtn;
      dispatch(setMessage(StringConstant.TOAST_LIKE_REMOVE));
      showToast();
    });
  };

  // Toast related activities
  const showToast = () => {
    setShowToast(true);
    setTimeout(() => {
      reloadPage();
    }, ValueConstant.TOAST_SHORT_TIMEOUT);
  };

  const hideToast = () => {
    setShowToast(false);
  };

  // Button element to show depends on the state
  let goingBtnToShow;
  let likeBtnToShow;

  const likeClickedBtn = (
    <div
      onClick={() => {
        return removeLike();
      }}
    >
      <img
        className="bottom-bar-icons bottom-bar-center-icon-touched"
        src={likeTouchedImg}
        alt="like-toucher-img"
      />
    </div>
  );

  const likeUnclickedBtn = (
    <div
      onClick={() => {
        return addLike();
      }}
    >
      <img
        className="bottom-bar-icons"
        src={likeUntouchedImg}
        alt="like-untouched-img"
      />
    </div>
  );

  const goingClickedBtn = (
    <div
      className="flex-container bottom-bar-join-button"
      onClick={() => {
        return removeParticipant();
      }}
    >
      <img
        className="bottom-bar-icons bottom-bar-right-btn"
        src={goingTouchedImg}
        alt="going-touched-img"
      />
      <h4 className="bottom-bar-join-txt">
        {StringConstant.BOTTOM_BAR_JOINED_STATE}
      </h4>
    </div>
  );

  const goingUnclickedBtn = (
    <div
      className="flex-container bottom-bar-join-button"
      onClick={() => {
        return addParticipant();
      }}
    >
      <img
        className="bottom-bar-icons bottom-bar-right-btn"
        src={goingUntouchedImg}
        alt="going-untouched-img"
      />
      <h4 className="bottom-bar-not-join-txt">
        {StringConstant.BOTTOM_BAR_JOIN_STATE}
      </h4>
    </div>
  );

  if (userActivity?.is_current_user_like === 1) likeBtnToShow = likeClickedBtn;
  else likeBtnToShow = likeUnclickedBtn;
  if (userActivity?.is_current_user_participant === 1)
    goingBtnToShow = goingClickedBtn;
  else goingBtnToShow = goingUnclickedBtn;

  return (
    <>
      <ToastComponent show={showToastData} hideToast={hideToast}>
        {message}
      </ToastComponent>
      <div className="bottom-nav-regular" id="bottom-bar-regular">
        <div className="flex-container flex-justify-content bottom-bar-btn">
          <div
            className="bottom-bar-left-icon"
            onClick={() => {
              return helperFunctions.showCommentBar();
            }}
          >
            <img
              className="bottom-bar-icons"
              src={commentUntouchedImg}
              alt="comment-img"
            />
          </div>
          <div className="bottom-bar-center-icon">{likeBtnToShow}</div>
          {goingBtnToShow}
        </div>
      </div>
    </>
  );
};

export default EventBottomBar;
