import { shallow } from "enzyme";
import EventContentComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<EventContentComponent />);
});

describe("<EventContentComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
