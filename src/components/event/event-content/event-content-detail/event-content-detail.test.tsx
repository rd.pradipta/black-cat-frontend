import { shallow } from "enzyme";
import EventContentDetailComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<EventContentDetailComponent />);
});

describe("<EventContentDetailComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
