import "./event-location.scss";
import StringConstant from "../../../../../helpers/constants/stringConstant";
import locationImg from "../../../../../assets/images/gmap.png";

const EventLocationComponent = ({ eventLocation }: any) => {
  return (
    <>
      <div className="line-separator horizontal-line " />
      <div className="event-detail-location-container">
        <div className="event-detail-location-txt-container">
          <div className="flex-container event-detail-location-header-txt-container">
            <div className="line-separator event-detail-header-txt-line" />
            <div className="event-detail-header-txt">
              {StringConstant.WHERE_HEADER}
            </div>
          </div>
          <div className="event-detail-location-top event-detail-location-txt">
            {StringConstant.OUR_FAVORITE_PLACE}
          </div>
          <div className="event-detail-location-txt">{eventLocation}</div>
        </div>
        <div className="event-detail-location-img-container">
          <img
            className="event-detail-location-img"
            src={locationImg}
            alt="location-img"
          />
        </div>
      </div>
    </>
  );
};

export default EventLocationComponent;
