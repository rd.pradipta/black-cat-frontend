import { shallow } from "enzyme";
import EventLocationComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<EventLocationComponent />);
});

describe("<EventLocationComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
