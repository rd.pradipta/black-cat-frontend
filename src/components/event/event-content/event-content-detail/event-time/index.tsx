import "./event-time.scss";
import helperFunctions from "../../../../../helpers/helperFunctions";
import StringConstant from "../../../../../helpers/constants/stringConstant";
import dateFromImg from "../../../../../assets/icons/date-from.svg";
import dateToImg from "../../../../../assets/icons/date-to.svg";

const EventTimeComponent = ({ startTime, endTime }: any) => {
  return (
    <>
      <div className="line-separator horizontal-line " />
      <div className="event-detail-time-container">
        <div className="flex-container">
          <div className="line-separator event-detail-header-txt-line" />
          <div className="event-detail-header-txt">
            {StringConstant.WHEN_HEADER}
          </div>
        </div>
        <br />
        <div className="container event-detail-time-centered">
          <div className="row">
            <div className="col">
              <div className="flex-container flex-center-content">
                <img
                  className="event-detail-time-img"
                  src={dateFromImg}
                  alt="date-from-img"
                />
                <h4 className="event-detail-time-date">
                  {helperFunctions.convertISODateToRegularDate(startTime, 1)}
                </h4>
              </div>
              <h1 className="event-detail-time-clock">
                {helperFunctions.convertISODateToRegularDate(startTime, 2)}
                <span className="event-detail-time-clock-identifier">
                  {helperFunctions.convertISODateToRegularDate(startTime, 3)}
                </span>
              </h1>
            </div>
            <div className="col-1">
              <div className="line-separator event-detail-time-vertical-line" />
            </div>
            <div className="col">
              <div className="flex-container flex-center-content">
                <img
                  className="event-detail-time-img"
                  src={dateToImg}
                  alt="date-to-img"
                />
                <h4 className="event-detail-time-date">
                  {helperFunctions.convertISODateToRegularDate(endTime, 1)}
                </h4>
              </div>
              <h1 className="event-detail-time-clock">
                {helperFunctions.convertISODateToRegularDate(startTime, 2)}
                <span className="event-detail-time-clock-identifier">
                  {helperFunctions.convertISODateToRegularDate(startTime, 3)}
                </span>
              </h1>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EventTimeComponent;
