/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "./event-content-detail.scss";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import EventContentDetailInterface from "../../../../interfaces/event/eventDetail";
import LoadingComponent from "../../../loading";
import EventDescriptionComponent from "./event-description";
import EventTimeComponent from "./event-time";
import EventLocationComponent from "./event-location";
import eventService from "../../../../services/eventService";

// Disini import detail box, location box, time box, dan juga fetch detail dari event untuk dikirim sebagai props ke komponen lain

const EventContentDetailComponent = () => {
  let componentToShow;
  const urlParam = useParams();
  const [loading, setLoading] = useState(true);
  const [eventData, setEvent] = useState<EventContentDetailInterface | null>(
    null
  );

  const getEvents = async () => {
    setLoading(true);
    await eventService
      .getDetail(urlParam.eventId!)
      .then(({ data }) => {
        setEvent(data);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getEvents();
  }, []);

  if (loading) componentToShow = <LoadingComponent />;
  else
    componentToShow = (
      <div id="event-content-detail-section" className="footer-content-bg">
        <EventDescriptionComponent
          eventDesc={eventData!.description}
          eventImgs={eventData!.images}
        />
        <EventTimeComponent
          startTime={eventData!.start_time}
          endTime={eventData!.end_time}
        />
        <EventLocationComponent eventLocation={eventData!.location} />
      </div>
    );

  return <div>{componentToShow}</div>;
};

export default EventContentDetailComponent;
