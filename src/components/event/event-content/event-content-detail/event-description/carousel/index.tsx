import "./carousel.scss";
import NumberConstant from "../../../../../../helpers/constants/numberConstant";

const CarouselComponent = ({ images }: any) => {
  const imgComponentList = [];

  for (
    let i = NumberConstant.ZERO;
    i < images.length;
    i += NumberConstant.ONE
  ) {
    imgComponentList.push(
      <img
        className="carousel-img-each"
        src={images[i]}
        key={i}
        alt="event-img"
      />
    );
  }

  return (
    <div id="slider">
      <figure>{imgComponentList}</figure>
    </div>
  );
};

export default CarouselComponent;
