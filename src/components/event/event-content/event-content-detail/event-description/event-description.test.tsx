import { shallow } from "enzyme";
import EventDescriptionComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<EventDescriptionComponent />);
});

describe("<EventDescriptionComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
