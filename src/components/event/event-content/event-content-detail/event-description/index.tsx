import "./event-description.scss";
import CarouselComponent from "./carousel";
import StringConstant from "../../../../../helpers/constants/stringConstant";
import helperFunctions from "../../../../../helpers/helperFunctions";

const EventDescriptionComponent = ({ eventDesc, eventImgs }: any) => {
  return (
    <>
      <div className="line-separator horizontal-line " />
      <div className="event-detail-desc-container">
        <CarouselComponent images={eventImgs} />
        <div className="event-detail-desc-txt-container">
          <div
            id="event-detail-desc-txt"
            className="event-detail-desc-txt event-detail-desc-txt-overflow"
          >
            {eventDesc}
          </div>
        </div>
        <div
          id="event-detail-desc-blur-container"
          className="event-detail-desc-txt-blur"
        >
          <div
            onClick={() => {
              return helperFunctions.showEventCompleteDesc();
            }}
            className="event-detail-desc-view-all-btn"
          >
            {StringConstant.EVENT_BTN_SHOW_ALL}
          </div>
        </div>
      </div>
    </>
  );
};

export default EventDescriptionComponent;
