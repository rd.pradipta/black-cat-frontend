import "./event-content.scss";
import EventNavbarComponent from "./event-navbar";
import EventContentDetailComponent from "./event-content-detail";
import EventContentParticipantComponent from "./event-content-participant";
import EventContentCommentComponent from "./event-content-comment";
import EventBottomBar from "./event-bottom-bar";

const EventContentComponent = () => {
  return (
    <div>
      <EventNavbarComponent />
      <EventContentDetailComponent />
      <EventContentParticipantComponent />
      <EventContentCommentComponent />
      <EventBottomBar />
    </div>
  );
};

export default EventContentComponent;
