import { shallow } from "enzyme";
import EventContentParticipantComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<EventContentParticipantComponent />);
});

describe("<EventContentParticipantComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
