/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "./event-content-participant.scss";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import LoadingComponent from "../../../loading";
import eventService from "../../../../services/eventService";
import StringConstant from "../../../../helpers/constants/stringConstant";
import NumberConstant from "../../../../helpers/constants/numberConstant";

// Assets
import likeUntouchedImg from "../../../../assets/icons/like-outline.svg";
import participantUntouchedImg from "../../../../assets/icons/check-outline.svg";

const EventContentParticipantComponent = () => {
  let componentToShow;
  const urlParam = useParams();
  const [loading, setLoading] = useState(true);
  const [participantImgs, setParticipantImgs] = useState([]);
  const [likesImgs, setLikesImgs] = useState([]);

  const createArrayOfImgs = (dataArr: any, type: string) => {
    const userImgArr = imgArrayBuilder(dataArr);
    if (type === "participants") {
      setParticipantImgs(userImgArr);
    } else {
      setLikesImgs(userImgArr);
    }
  };

  const getEventParticipants = async () => {
    setLoading(true);
    await eventService
      .getParticipants(urlParam.eventId!)
      .then(({ data }) => {
        createArrayOfImgs(data.participants, "participants");
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const getEventLikes = async () => {
    setLoading(true);
    await eventService
      .getLikes(urlParam.eventId!)
      .then(({ data }) => {
        createArrayOfImgs(data.likes, "likes");
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const imgArrayBuilder = (dataArr: any) => {
    const finalImgs: any = [];
    for (
      let i = NumberConstant.ZERO;
      i < dataArr.length;
      i += NumberConstant.ONE
    ) {
      finalImgs.push(
        <img
          className="participant-photo"
          src={dataArr[i].avatar}
          alt="people-img"
          key={i}
        />
      );
    }
    return finalImgs;
  };

  useEffect(() => {
    getEventParticipants();
    getEventLikes();
  }, []);

  if (loading) componentToShow = <LoadingComponent />;
  else
    componentToShow = (
      <div id="event-content-participant-section" className="footer-content-bg">
        <div className="line-separator horizontal-line" />
        <div className="participant-list-container">
          <img
            className="participant-card-spacing users-counter-icon"
            src={participantUntouchedImg}
            alt="participant-img"
          />
          <p className="participant-text participant-card-spacing">
            {participantImgs.length} {StringConstant.PARTICIPANT_SMALL}
          </p>
          <div className="participant-card-spacing participant-img-container wrap">
            {participantImgs}
          </div>
        </div>
        <div className="line-separator horizontal-line" />
        <div className="participant-list-container">
          <img
            className="participant-card-spacing users-counter-icon"
            src={likeUntouchedImg}
            alt="likes-img"
          />
          <p className="participant-text participant-card-spacing">
            {likesImgs.length} {StringConstant.LIKES_SMALL}
          </p>
          <div className="participant-card-spacing participant-img-container">
            {likesImgs}
          </div>
        </div>
        <div className="line-separator horizontal-line" />
      </div>
    );
  return <div>{componentToShow}</div>;
};

export default EventContentParticipantComponent;
