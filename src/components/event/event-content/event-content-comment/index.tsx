/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "./event-content-comment.scss";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import CommentBottomBar from "./comment-bottom-bar";
import eventService from "../../../../services/eventService";
import LoadingComponent from "../../../loading";
import StringConstant from "../../../../helpers/constants/stringConstant";
import NumberConstant from "../../../../helpers/constants/numberConstant";
import EventCommentCardComponent from "../../../card/event-comment-card";
import NoActivityComponent from "../../../no-activity";

const EventContentCommentComponent = () => {
  let componentToShow;
  const urlParam = useParams();
  const [loading, setLoading] = useState(true);
  const [comments, setComments] = useState([]);

  const localStorageData = JSON.parse(
    localStorage.getItem(StringConstant.KEY_LOCAL_STORAGE_LOGIN) || "{}"
  );
  const currentUserId = localStorageData.user_id;

  const getComments = async () => {
    setLoading(true);
    await eventService
      .getComments(urlParam.eventId!)
      .then(({ data }) => {
        setComments(data.comments);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getComments();
  }, []);

  const commentArr: any = [];
  if (!loading) {
    for (
      let i = NumberConstant.ZERO;
      i < comments.length;
      i += NumberConstant.ONE
    ) {
      commentArr.push(
        <EventCommentCardComponent
          key={i}
          commentData={comments[i]}
          currentUserId={currentUserId}
        />
      );
    }
  }

  if (loading) componentToShow = <LoadingComponent />;
  if (!loading && comments.length === NumberConstant.ZERO)
    componentToShow = <NoActivityComponent />;
  else if (!loading && comments.length) componentToShow = commentArr;
  return (
    <>
      <div
        id="event-content-comment-section"
        className="footer-content-bg event-content-comment-container"
      >
        {componentToShow}
      </div>
      <div className="separator-comment-bar-and-comment" />
      <CommentBottomBar />
    </>
  );
};

export default EventContentCommentComponent;
