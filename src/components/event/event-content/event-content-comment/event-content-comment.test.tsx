import { shallow } from "enzyme";
import EventContentCommentComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<EventContentCommentComponent />);
});

describe("<EventContentCommentComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
