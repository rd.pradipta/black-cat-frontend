import { shallow } from "enzyme";
import { Provider } from "react-redux";
import { mainStore } from "../../../../../store";
import CommentBottomBar from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(
    <Provider store={mainStore}>
      <CommentBottomBar />
    </Provider>
  );
});

describe("<CommentBottomBar /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
