/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "./comment-bottom-bar.scss";
import { useState } from "react";
import { useParams } from "react-router-dom";
import ToastComponent from "../../../../toast";
import eventService from "../../../../../services/eventService";
import StringConstant from "../../../../../helpers/constants/stringConstant";
import ValueConstant from "../../../../../helpers/constants/valueContant";
import helperFunctions from "../../../../../helpers/helperFunctions";

// Redux
import { useAppDispatch, useAppSelector } from "../../../../../hook";
import { setMessage } from "../../../../../actions/messageAction";

// Assets
import closeImg from "../../../../../assets/icons/cross.svg";
import sendImg from "../../../../../assets/icons/send.svg";

const CommentBottomBar = () => {
  const urlParam = useParams();
  const [commentData, setComment] = useState("");
  const [showToastData, setShowToast] = useState(false);

  const localStorageData = JSON.parse(
    localStorage.getItem(StringConstant.KEY_LOCAL_STORAGE_LOGIN) || "{}"
  );
  const currentUserId = localStorageData.user_id;

  const addComment = async () => {
    const payloadToSend = {
      user_id: currentUserId,
      comment: commentData,
    };
    await eventService.addComment(urlParam.eventId!, payloadToSend).then(() => {
      dispatch(setMessage(StringConstant.TOAST_COMMENT_POST));
      showToast();
    });
  };

  const handleChange = (event: any) => {
    setComment(event.target.value);
  };

  // Redux variables
  const dispatch = useAppDispatch();
  const { message } = useAppSelector(state => {
    return state.messageReducer;
  });
  const { usernameToReply } = useAppSelector(state => {
    return state.replyCommentReducer;
  });

  // Reload
  const reloadPage = () => {
    return window.location.reload();
  };

  // Toast related activities
  const showToast = () => {
    setShowToast(true);
    setTimeout(() => {
      reloadPage();
    }, ValueConstant.TOAST_SHORT_TIMEOUT);
  };

  const hideToast = () => {
    setShowToast(false);
  };

  return (
    <>
      <ToastComponent show={showToastData} hideToast={hideToast}>
        {message}
      </ToastComponent>
      <div
        className="comment-bar-container hide-content"
        id="bottom-bar-comment"
      >
        <div className="flex-container flex-justify-content comment-bar-btn">
          <div
            className="comment-bar-close-button"
            onClick={() => {
              return helperFunctions.showRegularBottomBar();
            }}
          >
            <img
              className="comment-bar-icons comment-bar-close-icon"
              src={closeImg}
              alt="close-comment-img"
            />
          </div>
          <div>
            <input
              type="text"
              className="comment-text-input"
              name="comment"
              placeholder={StringConstant.COMMENT_FIELD_EMPTY}
              value={commentData || usernameToReply}
              onChange={handleChange}
            />
          </div>
          <div
            className="flex-container comment-bar-submit-comment-button"
            onClick={() => {
              return addComment();
            }}
          >
            <img
              className="comment-bar-icons comment-bar-right-icon"
              src={sendImg}
              alt="send-comment-img"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default CommentBottomBar;
