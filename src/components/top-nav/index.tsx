import "./top-nav.scss";
import { useState } from "react";
import { Link } from "react-router-dom";
import NumberConstant from "../../helpers/constants/numberConstant";
import helperFunctions from "../../helpers/helperFunctions";

// Assets
import searchImg from "../../assets/icons/search.svg";
import logoCatImg from "../../assets/icons/logo-cat.svg";

const TopNavComponent = ({ avatarLink }: any) => {
  const [showSidebar, setShowSidebar] = useState(NumberConstant.ZERO);

  const showHideSidebar = () => {
    if (!showSidebar) {
      helperFunctions.showSidebar();
      setShowSidebar(NumberConstant.ONE);
    } else {
      helperFunctions.hideSidebar();
      setShowSidebar(NumberConstant.ZERO);
    }
  };

  return (
    <div className="top-nav">
      <div className="flex-container flex-justify-content">
        <div
          onClick={() => {
            return showHideSidebar();
          }}
        >
          <img
            className="navbar-img home-img"
            src={searchImg}
            alt="search-img"
          />
        </div>
        <Link to="/">
          <img className="navbar-img cat-img" src={logoCatImg} alt="cat-img" />
        </Link>
        <Link to="profile">
          <img
            className="navbar-img user-img"
            src={avatarLink}
            alt="user-img"
          />
        </Link>
      </div>
    </div>
  );
};

export default TopNavComponent;
