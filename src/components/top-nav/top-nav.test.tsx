import { shallow } from "enzyme";
import TopNavComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<TopNavComponent />);
});

describe("<TopNavComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
