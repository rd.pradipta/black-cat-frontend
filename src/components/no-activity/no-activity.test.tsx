import { shallow } from "enzyme";
import NoActivityComponent from ".";

let wrapper: any;
beforeEach(() => {
  wrapper = shallow(<NoActivityComponent />);
});

describe("<NoActivityComponent /> rendering", () => {
  it("Renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
