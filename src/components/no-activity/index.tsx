import "./no-activity.scss";
import StringConstant from "../../helpers/constants/stringConstant";
import logoNoActivityImg from "../../assets/icons/no-activity.svg";

const NoActivityComponent = () => {
  return (
    <div className="no-content-container">
      <img
        className="no-activity-img"
        src={logoNoActivityImg}
        alt="no-activity-img"
      />
      <p className="no-activity-txt">{StringConstant.NO_ACTIVITY_FOUND}</p>
    </div>
  );
};

export default NoActivityComponent;
