import "./event-comment-card.scss";
import helperFunctions from "../../../helpers/helperFunctions";

// Redux
import { useAppDispatch } from "../../../hook";
import commentReplyAction from "../../../actions/commentReplyAction";

// Assets
import replyImg from "../../../assets/icons/reply.svg";

const EventCommentCardComponent = ({ commentData }: any) => {
  // Redux variables
  const dispatch = useAppDispatch();

  // Dispatch comment reply to send a person username that being replied
  const replySomeone = () => {
    const usernameMention = `@${commentData.creator.username}`;
    dispatch(commentReplyAction.replyComment(usernameMention));
    helperFunctions.showCommentBar();
  };

  return (
    <div className="comment-card-container">
      <div className="flex-container comment-card-author-info-container">
        <img
          className="comment-card-author-photos"
          src={commentData.creator.avatar}
          alt="author-img"
        />
        <div className="event-header-author-name-date-container">
          <div className="flex-container flex-justify-content">
            <div className="flex-container">
              <div className="comment-card-author-username">
                {commentData.creator.username}
              </div>
              <div className="comment-card-author-posted-time">
                {helperFunctions.getTimeDifferencesFromNow(
                  commentData.create_at
                )}
              </div>
            </div>
            <div
              className="comment-card-reply-btn"
              onClick={() => {
                return replySomeone();
              }}
            >
              <img
                className="comment-card-reply-icon"
                src={replyImg}
                alt="reply-img"
              />
            </div>
          </div>
          <div className="comment-card-author-comment">
            {commentData.comment}
          </div>
        </div>
      </div>
    </div>
  );
};

export default EventCommentCardComponent;
