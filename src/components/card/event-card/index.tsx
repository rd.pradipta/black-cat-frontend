/* eslint-disable react/destructuring-assignment */
import "./event-card.scss";
import { Link } from "react-router-dom";
import StringConstant from "../../../helpers/constants/stringConstant";
import helperFunctions from "../../../helpers/helperFunctions";

// Service
import eventService from "../../../services/eventService";

// Assets
import timeImg from "../../../assets/icons/time.svg";
import likeUntouchedImg from "../../../assets/icons/like-outline.svg";
import goingUntouchedImg from "../../../assets/icons/check-outline.svg";
import likeTouchedImg from "../../../assets/icons/like.svg";
import goingTouchedImg from "../../../assets/icons/check.svg";

const EventCardComponent = ({ eventData, currentUserId }: any) => {
  const linkToEventDetail = "/dashboard/event/".concat(eventData.id);

  const payloadToSend = {
    user_id: currentUserId,
  };

  // Button element to show depends on the state
  let goingBtnToShow;
  let likeBtnToShow;

  // Unclicked button, click to add as participant
  const goingUnclickedBtn = (
    <div
      className="flex-container event-action-buttons-container"
      onClick={() => {
        return addParticipant();
      }}
    >
      <img
        className="event-time-and-action-photo action-photo-idle"
        src={goingUntouchedImg}
        alt="going-untouched-img"
      />
      <div className="event-time-and-action-info">
        {eventData.participants_count} {StringConstant.PARTICIPANT}
      </div>
    </div>
  );

  const goingClickedBtn = (
    <div
      className="flex-container event-action-buttons-container"
      onClick={() => {
        return removeParticipant();
      }}
    >
      <img
        className="event-time-and-action-photo action-photo-clicked-going"
        src={goingTouchedImg}
        alt="going-touched-img"
      />
      <div className="event-time-and-action-info action-info-clicked">
        {StringConstant.I_AM_GOING}
      </div>
    </div>
  );

  // Unclicked button, click to like an event
  const likeUnclickedBtn = (
    <div
      className="flex-container event-action-buttons-container"
      onClick={() => {
        return addLike();
      }}
    >
      <img
        className="event-time-and-action-photo action-photo-idle"
        src={likeUntouchedImg}
        alt="like-untouched-img"
      />
      <div className="event-time-and-action-info">
        {eventData.likes_count} {StringConstant.LIKES}
      </div>
    </div>
  );

  const likeClickedBtn = (
    <div
      className="flex-container event-action-buttons-container"
      onClick={() => {
        return removeLike();
      }}
    >
      <img
        className="event-time-and-action-photo action-photo-clicked-like"
        src={likeTouchedImg}
        alt="like-touched-img"
      />
      <div className="event-time-and-action-info action-info-clicked">
        {StringConstant.I_LIKE_IT}
      </div>
    </div>
  );

  // Reload
  const reloadPage = () => {
    return window.location.reload();
  };

  // Actions related to participation & likes

  const addParticipant = async () => {
    await eventService.addParticipant(eventData.id, payloadToSend).then(() => {
      goingBtnToShow = goingClickedBtn;
      reloadPage();
    });
  };

  const removeParticipant = async () => {
    await eventService
      .removeParticipant(eventData.id, currentUserId)
      .then(() => {
        goingBtnToShow = goingUnclickedBtn;
        reloadPage();
      });
  };

  const addLike = async () => {
    await eventService.addLike(eventData.id, payloadToSend).then(() => {
      likeBtnToShow = likeClickedBtn;
      reloadPage();
    });
  };

  const removeLike = async () => {
    await eventService.removeLike(eventData.id, currentUserId).then(() => {
      likeBtnToShow = likeUnclickedBtn;
      reloadPage();
    });
  };

  if (eventData.is_current_user_participant === 1)
    goingBtnToShow = goingClickedBtn;
  else goingBtnToShow = goingUnclickedBtn;
  if (eventData.is_current_user_like === 1) likeBtnToShow = likeClickedBtn;
  else likeBtnToShow = likeUnclickedBtn;

  let eventThumbnailToShow;
  if (eventData.thumbnail)
    eventThumbnailToShow = (
      <img
        className="event-card-thumbnail-image-container"
        src={eventData.thumbnail}
        alt="event-img"
      />
    );

  return (
    <>
      <div className="line-separator horizontal-line " />
      <div className="event-card-container">
        <Link to={linkToEventDetail} className="remove-link-decoration">
          <div className="flex-container flex-justify-content">
            <div className="flex-container event-author-username-photo-container">
              <img
                className="event-author-photo"
                src={eventData.creator.avatar}
                alt="author-img"
              />
              <div className="event-author-username">
                {eventData.creator.username}
              </div>
            </div>
            <div className="channel-name-container">
              {eventData.channel_name}
            </div>
          </div>
          <br />
          <div className="flex-container flex-justify-content">
            <div>
              <h3 className="event-title">{eventData.title}</h3>
              <div className="flex-container event-time-photo-container">
                <img
                  className="event-time-and-action-photo action-photo-idle"
                  src={timeImg}
                  alt="time-img"
                />
                <div className="event-time-and-action-info">
                  {helperFunctions.convertISODateToRegularDate(
                    eventData.start_time,
                    0
                  )}{" "}
                  -{" "}
                  {helperFunctions.convertISODateToRegularDate(
                    eventData.end_time,
                    0
                  )}
                </div>
              </div>
            </div>
            {eventThumbnailToShow}
          </div>
          <h5 className="event-short-description desc-overflow">
            {eventData.description}
          </h5>
        </Link>
        <div className="flex-container">
          {goingBtnToShow}
          {likeBtnToShow}
        </div>
      </div>
      <div className="line-separator horizontal-line" />
    </>
  );
};

export default EventCardComponent;
