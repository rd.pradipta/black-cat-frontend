import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./reducers/authReducer";
import messageReducer from "./reducers/messageReducer";
import replyCommentReducer from "./reducers/commentReplyReducer";

export const mainStore = configureStore({
  reducer: {
    authReducer,
    messageReducer,
    replyCommentReducer,
  },
});

export type RootState = ReturnType<typeof mainStore.getState>;
export type AppDispatch = typeof mainStore.dispatch;
