# Black Cat

Single Page App Based Social Event Sharing Platform

## Documentation

Please refer to this [documentation link](https://confluence.shopee.io/x/KdFiVg) for more information

Backend Project please [click this link](https://gitlab.com/rd.pradipta/black-cat-backend)

## Project Usage

### `yarn install && yarn start:dev`

Install all required dependencies & start the project in development configurations. Change the word `dev` to `prod` to use the production configurations

### `yarn run lint:all`

Run ESLint (Typescript linter) and Stylelint (SCSS linter) on the project. That command will return errors related to code style

### `yarn run format:fix`

Run Prettier to check the code formatting issues and fix them.

### `yarn run build`

Build the project into bundle of files that can be deployed to the production

## Checklists

Here are the checklists of the project requirements. Status will be updated frequently

### Git

- [x] Must create a git repo on gitlab
- [x] Commit frequently
- [x] Commit message should be meaningful
- [x] (Bonus) set up gitlab CI checks for the linting, testing job

### Linting

- [x] Setup ESLint as a `pre-commit` hook
- [x] Use `prettier` to format your code automatically
- [x] Use `stylelint` as a `pre-commit` hook to lint your CSS code as well

### React

- [x] Must use React 16+ for the entry task (sorry)
- [x] Use `react-router` for the routing
- [ ] (Bonus) Explore using newer React APIs like Portal, Context and Lazy

### API Design

- [x] Must design and document backend APIs to retrieve all necessary data for FE user interactions like login, browsing etc
- [x] Decide how you are going to implement session, using sessionid vs token based method
- [x] Create your own mock data using faker.js or use https://mockaroo.com/
- [ ] (Bonus) Create a node express server to test out the session logic.

### Dev and Build

- [x] Use `create-react-app` as your build tool.
- [x] Should have separate configurations for local development and production build.
- [x] Implement Infinite Scroll on Activity List

### Redux

- [x] Must use `redux` to manage your data logic
- [ ] Optional use `redux-thunk` for async actions
- [ ] (Bonus) Support inject reducer asynchronously on demand instead of combine all the reducers up front

### JavaScript / Typescript

- [x] Use modern ECMAScript syntax like class, module etc
- [x] Use async/await, generator functions to avoid the callback hell
- [x] Use typescript
- [x] Must break the state into different types
- [x] Must create the response into different types
- [x] Must create the request into different types
- [x] (Bonus) Study the min browser version you plan to support and only include the necessary polyfills to save bundle size

### CSS

- [x] Use CSS, inline-style or styled component is allowed,
- [x] Must not polutte the .tsx file with the css or style-component definition. implementation of style must be done in different files
- [ ] (Bonus) Add autoPrefixer to cover browser discrepancies on CSS implementation

### Testing

- [x] Set up `jest` and `enzyme` as your test framework
- [x] Write at least one snapshot test
- [x] Use enzyme to test out the rendering logic for at least one React component
- [x] Must not polutte the view with logic, unless the logic is minimal; medium to complex logic must be created under action

### Bonus

- [ ] Support internationalisation (i18n)
- [ ] Annotate page properly with SEO meta tags
